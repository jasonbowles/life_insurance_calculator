## Life Insurance Needs Analysis - Calculator

![LifeNeedsPic](static/lifeNeeds.PNG)

[Demo](http://life-insurance-calculator.appspot.com/viz)

### Background

This was done as part of a hackathon and I've re-purposed the source code to run on google's platform